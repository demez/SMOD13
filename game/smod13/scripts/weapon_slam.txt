// Slam

WeaponData
{
	// Weapon data is loaded by both the Game and Client DLLs.
	"printname"	"#HL2_SLAM"
	"viewmodel"				"models/weapons/v_slam.mdl"
	"playermodel"			"models/weapons/w_slam.mdl"
	"anim_prefix"			"slam"
	"bucket"				"grenades"
	"bucket_position"		"1"

	"clip_size"				"3"
	"primary_ammo"			"None"
	"secondary_ammo"		"slam"
	"default_clip2"			"3"

	"item_flags"			"2"

	// Weapon Sprite data is loaded by the Client DLL.
	TextureData
	{
		"weapon"
		{
				"font"		"SMODWeaponIcons"
				"character"	"r"
		}
		"weapon_s"
		{	
				"font"		"SMODWeaponIconsSelected"
				"character"	"r"
		}
		"weapon_small"
		{	
				"font"		"SMODWeaponIconsSmall"
				"character"	"r"
		}
		"ammo2"
		{
				"font"		"WeaponIconsSmall"
				"character"	"o"
		}
		"crosshair"
		{
				"file"		"sprites/crosshairs"
				"x"			"0"
				"y"			"48"
				"width"		"24"
				"height"	"24"
		}
		"autoaim"
		{
				"file"		"sprites/crosshairs"
				"x"			"48"
				"y"			"72"
				"width"		"24"
				"height"	"24"
		}
	}
}