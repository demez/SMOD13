// Shotgun

WeaponData
{
	// Weapon data is loaded by both the Game and Client DLLs.
	"printname"		"#HL2_Shotgun"
	"viewmodel"		"models/v_shotgun.mdl"
	"playermodel"		"models/w_shotgun.mdl"
	"anim_prefix"		"shotgun"
	"bucket"			"heavy"
	"bucket_position"	"20"

	"clip_size"			"8"
	"default_clip"		"12"
	"primary_ammo"		"Buckshot"
	"secondary_ammo"	"None"

	"weight"		"15"
	"item_flags"		"0"
	"ITEM_FLAG_NOAUTOSWITCHEMPTY"	"1"
	"addfov"				"36" 
	
	"CanIronsight"	"1"
	"IronSight"
	{
		"forward"	"-5.5"		//x
		"right"		"-3.97"		//y
		"up"		"5.8"		//z
		"pitch"		"0"
		"yaw"		"4"
		"roll"		"3"
		"fov"		"-20"
	}

	// Sounds for the weapon. There is a max of 16 sounds per category (i.e. max 16 "single_shot" sounds)
	SoundData
	{
		"empty"			"Weapons.Empty"
		"reload"		"Weapon_Shotgun.Reload"
		"special1"		"Weapon_Shotgun.Special1"
		"single_shot"		"Weapon_Shotgun.Single"
		"double_shot"		"Weapon_Shotgun.Double"
	}

	// Weapon Sprite data is loaded by the Client DLL.
	TextureData
	{
		"weapon"
		{
			"file"		"sprites/640hud1"
			"x"			"0"
			"y"			"180"
			"width"		"170"
			"height"	"45"
		}
		"weapon_s"
		{	
			"file"		"sprites/640hud4"
			"x"			"0"
			"y"			"180"
			"width"		"170"
			"height"	"45"
		}
		"ammo"
		{
				"font"		"WeaponIconsSmall"
				"character"	"s"
		}
		"crosshair"
		{
				"font"		"Crosshairs"
				"character"	"Q"
		}
		"autoaim"
		{
			"file"		"sprites/crosshairs"
			"x"			"0"
			"y"			"48"
			"width"		"24"
			"height"	"24"
		}
	}
}