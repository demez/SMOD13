//Custom Weapon Example Script File
//This example weapon will a SMG that fires a rocket.
//The entities are linked to the same name as the file, so in this case, it would be "example_rocket_smg".

WeaponData
{
	//Default Half-Life 2 weapon settings
	
	"printname"				"EXAMPLE WEAPON:\nROCKET SMG"	//The name of the weapon on the hud. It doesn't HAVE to be uppercase, it just matches with the stock weapons if it is.
	"viewmodel"				"models/v_9mmAR.mdl"			//Model used in first person.
	"playermodel"			"models/weapons/w_smg_mp5.mdl"	//Model used in the world, by NPCs or in 3rd person.

	"bucket"				"smgrifle"	//Position in the hud. Vaild names are "melee", "pistol", "smgrifle", "shotgunsniper", "grenades", "special" and "tools". Set it to "disabled" to disable it.
	"bucket_position"		"12"		//Position in THAT hud. Larger the number the lower down it is.

	"clip_size"				"30"	//Mag size for the primary attack. -1 means to use directly from the carried ammo.
	"default_clip"			"30"	//How much of the primary ammo the weapon spawns with (not including what's in the mag, that is ALWAYS given). -1 = None.
	"primary_ammo"			"SMG1"	//Type of ammo the primary attack uses. None means not to use any.
	
	"clip2_size"			"-1"		//Mag size for the secondary attack. -1 means to use directly from the carried ammo.
	"default_clip2"			"-1"		//How much of the secondary ammo the weapon spawns with (not including what's in the mag, that is ALWAYS given). -1 = None.
	"secondary_ammo"		"rpg_round"	//Type of ammo the secondary attack uses. None means not to use any.

	"item_flags"			"0"	//Weapon's flags.
	//1 = Allows you to select it when there is no ammo (for melee weapons, or guns with special functions)
	//2 = Don't reload without player imput.
	//16 = When the ammo runs out ot make the weapon disapear (best for grenades).
	//You can mix and match these. 18 would make it not auto-reload and remove it when you're out of ammo for it.
	
	// Sounds for the weapon. There is a max of 16 sounds per category (i.e. max 16 "single_shot" sounds)
	"SoundData"
	{
		"empty"				"Weapon_SMG1.Empty"			//Sound when attemping to fire and the mag is empty.
		"single_shot"		"Weapon_RocketSMG.Single"		//Sound for the primary attack firing.
		"single_shot_npc"	"Weapon_RocketSMG.Single"	//Sound for NPC primary atttack.
		"double_shot"		"Weapon_SMG1.Double"		//Sound for the secondary attack firing
		"double_shot_npc"	"Weapon_SMG1.Double"		//Sound for NPC secondary attack.
		"reload"			"Weapon_SMG1.Reload"		//Reload sound.
		"reload_npc"		"Weapon_SMG1.NPC_Reload"	//NPC reload sound.
		"pump"				"Weapon_Shotgun.Pump"		//Shotgun pump sound
		"deploy"			"Weapon_DEagle.Deploy"		//Sound when deploying the weapon.
	}

	// All this stuff controls is the sprites on the player's hud. Can be actual sprites like in Half-Life: Source or fonts like in Half-Life 2 and Counter-Strike: Source.
	"TextureData"
	{
		"weapon"									//Weapon icon
		{
				"font"		"WeaponIcons"
				"character"	"a"
		}
		"weapon_s"									//Weapon icon's glow (should be the same as above)
		{	
				"font"		"WeaponIconsSelected"
				"character"	"a"
		}
		"weapon_small"								//Weapon icon when picked up
		{
				"font"		"WeaponIconsSmall"
				"character"	"a"
		}
		"ammo"										//Primary ammo icon
		{
				"font"		"WeaponIconsSmall"
				"character"	"r"
		}
		"ammo2"										//Secondary ammo icon
		{
				"font"		"WeaponIconsSmall"
				"character"	"t"
		}
		"crosshair"									//Don't touch this
		{
				"font"		"Crosshairs"
				"character"	"Q"
		}
		"autoaim"									//Or this
		{
				"file"		"sprites/crosshairs"
				"x"			"0"
				"y"			"48"
				"width"		"24"
				"height"	"24"
		}
	}

	//SMOD new weapon settings
	
	"BuiltRightHanded"			"1"					//If 0, it will flip to the other side of the screen
	"addfov"					"36"				//Viewmodel fov adjustment. Add to it to increase it and minus from it to lower it. Setting it to 20 is best for most Counter-Strike: Source models.
	//An FOV of 54 is default on HL2 weapon models, which then you'd leave it to 0. Setting it to 20 would bring that to 74, which is used in CS:S. Setting it to 36 is best for most models ported from HL1.
	"MuzzleFlashAttachement"	"muzzle"			//Muzzle flash attachement point. For most HL2 weapons it would be "muzzle" and for most CS:S weapons it would be "1"
	"UsesCSMuzzleFlash"			"0"					//0 will make it use HL2's effect based flash. 1 will make it use CS:S's sprite based flash.
	"CSMuzzleFlashType"			"CS_MUZZLEFLASH"	//Which sprite based flash to use. Valid is "CS_MUZZLEFLASH" or "CS_MUZZLEFLASH_X".

	"CanIronsight"	"1"	//If 1 this weapon can ironsight
	"IronSight"			//Ironsight settings. To see how these settings will end up without having to close the game, edit this file, open the game again, repeat, you can use the viewmodel_adjust_ commands.
	{
		"forward"	"-4"		//x
		"right"		"-2.6"		//y
		"up"		"2.3"		//z
		"pitch"		"0"
		"yaw"		"0"
		"roll"		"0"
		"fov"		"-20"
	}
	
	"IsScriptedWeapon"			"1"	//This needs to be 1 or else the WeaponSettings block won't do anything
	
	"WeaponSettings"	//This is the block for the custom weapon details and settings.
	{
		"precache_entity"	"rpg_missile"	//use this to precache the entity your weapon launches (if it does launch one)
		"precache_entity2"	"grenade_ar2"	//since the primary and secondary fires can fire different stuff, you may want to precache another entity.
		
		"ReloadsSingly"			"0"	//If 1, it will only reload a single bit of ammo at a time, like the shotgun. 
		"UsesShotgunReload"		"0"	//If 1, it will play ACT_VM_RELOAD_START, then play the reload animation (looping if ReloadsSingly is 1), and then ACT_VM_RELOAD_END.
		
		"PlayerAnimation"	"smg"	//Animation set used by the player when using this. Vaild are "smg", "pistol", "crowbar", "rifle", "shotgun", "slam", "physcannon", "grenade", "akimbo", "knife".
		
		"Animations" //The animations played when using the weapon. If your weapon does not use X animation, feel free to not include it.
		{
			"deploy"		"ACT_VM_DRAW"				//Animation to play when deploying.
			"reload"		"ACT_VM_RELOAD"				//Animation to play when reloading.
			"idle"			"ACT_VM_IDLE"				//Animation to play when idle.
			"primary"		"ACT_VM_PRIMARYATTACK"		//Animation to play when using primary attack.
			"primiss"		"ACT_VM_MISSCENTER"			//Animation to play when missing with the primary attack (melee).
			"secondary"		"ACT_VM_SECONDARYATTACK"	//Animation to play when using secondary attack.
			"secmiss"		"ACT_VM_MISSCENTER2"		//Animation to play when missing with the secondary attack (melee).
			"grenadehigh"	"ACT_VM_THROW"				//Animation to play when throwing a grenade.
			"grenadelow"	"ACT_VM_HAULBACK"			//Animation to play with dropping a grenade.
			"gerpullhigh"	"ACT_VM_PULLBACK_HIGH"		//Animation to play when cooking a ready-to-be-thrown grenade.
			"gerpulllow"	"ACT_VM_PULLBACK_LOW"		//Animation to play when cooking a ready-to-be-dropped grenade.
		}
		
		"PrimaryAttack"
		{
			"Type"			"Bullet"//Type of attack this is. "Bullet" for normal guns, "Launch" to fire an ent, "Grenade" to throw/drop ents and "Melee" for a melee swing.
			"Damage"		"4"		//Damage the attack does
			"UnderWater"	"0"		//If 1 this attack will be able to fire underwater.
			"Pump"			"0"		//If 1, after this attack, it will play ACT_VM_PUMP and play the "pump" sound.
			"FireRate"		"0.80"	//Fire rate, the lower this number the faster it can fire again. This is in seconds.
			
			"Bullet"	//Details for the chosen attack type.
			{
				"Mode"		"Auto"	//Fire Mode. Semi for single shot, Auto for full-auto. Burst for 3 round burst but that don't work yet.
				"Amount"	"1"		//Amount of bullets to fire. Unless you're doing shotguns or something stupid leave this to 1.
				"Range"		"4096"	//Max range the bullet can/will fly before it disapears. Only set it lower then 4096 if you're doing shotguns (which in that case it should be 1024)
				"Tracer"	"0"		//Tracer options. Leave to 0 to use the default. Vaild are "AR2Tracer", "HelicopterTracer", "AirboatGunTracer" and "AirboatGunHeavyTracer".
				"Spread"			//Bullet spread vectors. These equel VECTOR_CONE numbers. So for VECTOR_CONE_5DEGREES set each to 5.
				{
					"x"	"5" //Normal spread.
					"y"	"5"
					"z"	"5"
					
					"Ironsight"	//Spread when ironsighted. You only need to include this if you want your gun to have a different spread when ironsighted.
					{
						"x"	"4"
						"y"	"4"
						"z"	"4"
					}
				}
			}
		}
		
		"SecondaryAttack" //This is the block for the secondary attack. If you do not want a secondary attack, just simply exclude this block.
		{
			"Type"			"Launch"	//Alongisde of the same types that the PrimaryAttack has, setting this to "Scope" will give you the same scope that's on weapon_sniperrifle. There is no settings for that, however.
			"UnderWater"	"0"
			"Pump"			"0"
			"FireRate"		"3"
			"Damage"		"200"
			"Launch"
			{
				"Entity"		"rpg_missle"	//Entity to launch.
				"Launch_Type"	"rpg"			//Launch type. "smg1" for an arc and "rpg" for straight forward.
				"Velocity"		"1500"			//Velocity of launched ent. SMG1 Grenades fly at 400 and RPG rockets fly at 1500.
				
				"Model"			"models/weapons/w_missile_launch.mdl"	//Model for launched ent. (only for stuff like prop_physics. pre-set entities like grenade_ar2 override this.)
			}
		}
		
		"NPC" //This is the block for NPCs. If you do not want NPCs using this weapon, simply exclude this block.
		{
			"Type"		"Bullet"	//NPCs only support "Bullet" and "Melee"
			"Damage"	"3"			//Damage from NPCs using this weapon
			"Animation"	"smg"		//Animation set used by NPCs when using this. Vaild are "smg", "pistol", "crowbar", "rifle" and "shotgun".
			
			"Bullet"
			{
				"MaxRange"	"1400"	//This is the maxium range NPCs can be at before they will need to get closer to fire. NPCs with their Classify set to CLASS_PLAYER_ALLY has this value doubled.
				"MinRange"	"0"		//This is the minium range NPCs can be at before they will need to get further away to fire. Having this be 0 means they can always fire.
				"Amount"	"1"
				"Tracer"	"0"
				"Spread"	//Should probably be the same as your normal attack's spread
				{
					"x"	"5"
					"y"	"5"
					"z"	"5"
				}
			}
		}
	}
}