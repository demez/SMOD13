// LASER GUN

WeaponData
{
	// Weapon data is loaded by both the Game and Client DLLs.
	"printname"			"#SMOD_LaserGun"
	"viewmodel"			"models/weapons/v_laser.mdl"
	"playermodel"			"models/weapons/w_laser.mdl"
	"anim_prefix"			"ar2"
	"bucket"			"2"
	"bucket_position"		"3"

	"clip_size"			"1"
	"default_clip"			"1"

//	"clip2_size"			"1"
//	"default_clip2"			"0"

	"weight"			"4"
	"item_flags"			"0"

	// Sounds for the weapon. There is a max of 16 sounds per category (i.e. max 16 "single_shot" sounds)
	SoundData
	{
		"special1"		"Weapon_CombineGuard.Special1"
		"empty"			"Weapon_IRifle.Empty"
		//"reload"		"Weapon_AR2.Reload"
		"single_shot"		"Weapon_Lasergun.LaserOn"
		"single_shot_npc"	"Weapon_Lasergun.LaserOn_NPC"
	}

	// Weapon Sprite data is loaded by the Client DLL.
	TextureData
	{
		"weapon"
		{
			"file"		"sprites/hud_weapons_redux"
			"x"			"72"
			"y"			"96"
			"width"		"64"
			"height"	"32"
		}
		"weapon_s"
		{
			"file"		"sprites/hud_weapons_redux"
			"x"			"72"
			"y"			"96"
			"width"		"64"
			"height"	"32"
		}
		"ammo"
		{
				"font"		"WeaponIcons"
				"character"	"u"
		}
		"crosshair"
		{
				"font"		"Crosshairs"
				"character"	"Q"
		}
		"autoaim"
		{
				"file"		"sprites/crosshairs"
				"x"			"0"
				"y"			"48"
				"width"		"24"
				"height"	"24"
		}
	}
}
